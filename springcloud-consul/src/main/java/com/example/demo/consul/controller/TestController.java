package com.example.demo.consul.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 路径：com.example.demo.consul.controller
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/21 16:55
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@RestController
public class TestController {

    @GetMapping("/test")
    public String test(){
        return "Holle World!";
    }
}

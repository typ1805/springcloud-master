package com.example.demo.sleuth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 路径：com.example.demo.sleuth.controller
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/18 22:39
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@RestController
public class TestMiyaController {

    private static final Logger log = LoggerFactory.getLogger(TestMiyaController.class);

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/test")
    public String test(){
        log.info("test is being called");
        return "I'm miya!";
    }

    @GetMapping("/miya")
    public String info(){
        log.info("info is being called");
        return restTemplate.getForObject("http://localhost:8081/info",String.class);
    }


}

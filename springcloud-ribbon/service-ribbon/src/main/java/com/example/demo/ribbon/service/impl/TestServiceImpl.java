package com.example.demo.ribbon.service.impl;

import com.example.demo.ribbon.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * 路径：com.example.demo.ribbon.service.impl
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/11 14:57
 * 修改人：
 * 修改备注：
 * 修改时间：
 */

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    RestTemplate restTemplate;

    @Override
    public String test(String name) {
        return restTemplate.getForObject("http://RIBBON-CLIENT/holle?name="+name,String.class);
    }
}

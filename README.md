# springcloud-master

#### 项目介绍

springcloud全家桶实例搭建

码农下载：https://gitee.com/MoCunXiaoPing/springcloud-master

#### 软件架构

[SpringCloud——服务的注册与发现Eureka](https://blog.csdn.net/typ1805/article/details/82621881)

[SpringCloud——客户端负载平衡器Ribbon](https://blog.csdn.net/typ1805/article/details/82626893)

[SpringCloud——声明性REST客户端Feign](https://blog.csdn.net/typ1805/article/details/82633908)

[SpringCloud——断路器Hystrix](https://blog.csdn.net/typ1805/article/details/82682504)

[SpringCloud——路由器和过滤器Zuul](https://blog.csdn.net/typ1805/article/details/82687655)

[SpringCloud——分布式配置中心Spring Cloud Config](https://blog.csdn.net/typ1805/article/details/82702223)

[SpringCloud——消息总线Bus](https://blog.csdn.net/typ1805/article/details/82764084)

[SpringCloud——分布式跟踪Sleuth](https://blog.csdn.net/typ1805/article/details/82778168)

[SpringCloud——服务注册consul](https://blog.csdn.net/typ1805/article/details/82817451)

[SpringCloud——消息驱动Stream](https://blog.csdn.net/typ1805/article/details/82841037)

[SpringCloud——注册中心Zookeeper](https://blog.csdn.net/typ1805/article/details/82849855)

[SpringCloud——安全认证Security](https://blog.csdn.net/typ1805/article/details/82886144)


#### CSDN博客地址
 [https://blog.csdn.net/typ1805/](https://blog.csdn.net/typ1805/)

### 关于关注

![个人公众号](https://images.gitee.com/uploads/images/2020/0419/152754_c2f5e264_1739235.jpeg "个人公众号")
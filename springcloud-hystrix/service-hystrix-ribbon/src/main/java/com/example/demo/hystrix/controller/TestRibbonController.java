package com.example.demo.hystrix.controller;

import com.example.demo.hystrix.servcie.TestRibbonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 路径：com.example.demo.hystrix.controller
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/12 16:18
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@RestController
public class TestRibbonController {

    @Autowired
    private TestRibbonService testRibbonService;

    @GetMapping("/test")
    public String test(String name){
        return testRibbonService.test(name);
    }
}

package com.example.demo.hystrix.service;

import org.springframework.stereotype.Component;

/**
 * 路径：com.example.demo.hystrix.service.impl
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/12 16:41
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@Component
public class TestFeignServiceFallback implements TestFeignService {

    @Override
    public String getByClientOne(String name) {
        return name+" error!";
    }
}

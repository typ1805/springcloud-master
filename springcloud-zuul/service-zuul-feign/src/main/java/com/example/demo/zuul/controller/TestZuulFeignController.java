package com.example.demo.zuul.controller;

import com.example.demo.zuul.service.TestZuulFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 路径：com.example.demo.zuul.controller
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/13 10:56
 * 修改人：
 * 修改备注：
 * 修改时间：
 */

@RestController
public class TestZuulFeignController {

    @Autowired
    private TestZuulFeignService testZuulFeignService;

    @GetMapping("/test")
    public String test(String name){
        return testZuulFeignService.getByClientOne(name);
    }
}

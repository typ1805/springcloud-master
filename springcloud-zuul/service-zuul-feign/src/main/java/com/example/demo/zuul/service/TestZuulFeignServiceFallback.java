package com.example.demo.zuul.service;

import org.springframework.stereotype.Component;

/**
 * 路径：com.example.demo.zuul.service
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/13 10:55
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@Component
public class TestZuulFeignServiceFallback implements TestZuulFeignService{

    @Override
    public String getByClientOne(String name) {
        return name + " error!";
    }
}

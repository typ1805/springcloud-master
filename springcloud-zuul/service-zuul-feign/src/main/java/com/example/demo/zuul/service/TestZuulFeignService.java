package com.example.demo.zuul.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 路径：com.example.demo.zuul.service
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/13 10:53
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@FeignClient(value = "zuul-client", fallback = TestZuulFeignServiceFallback.class )
public interface TestZuulFeignService {

    @RequestMapping(value = "/holle",method = RequestMethod.GET)
    public String getByClientOne(@RequestParam(value = "name")String name);
}

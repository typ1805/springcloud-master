package com.example.demo.zuul.service.impl;

import com.example.demo.zuul.service.TestZuulRibbonService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * 路径：com.example.demo.zuul.service.impl
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/13 11:09
 * 修改人：
 * 修改备注：
 * 修改时间：
 */

@Service
public class TestZuulRibbonServiceImpl implements TestZuulRibbonService {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "error")
    public String test(String name) {
        return restTemplate.getForObject("http://ZUUL-CLIENT/holle?name=" + name, String.class);
    }

    public String error(String name) {
        return name + " error !";
    }
}

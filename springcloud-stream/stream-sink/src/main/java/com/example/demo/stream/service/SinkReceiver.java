package com.example.demo.stream.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * 路径：com.example.demo.stream.service
 * 类名：
 * 功能：用来指定一个或多个定义了@Input或@Output注解的接口
 * 备注：消费者
 * 创建人：typ
 * 创建时间：2018/9/25 14:45
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@EnableBinding(value = {Sink.class})
public class SinkReceiver {

    private static final Logger log = LoggerFactory.getLogger(SinkReceiver.class);

    /**
     * 方法名：
     * 功能：将被修饰的方法注册到消息中间件上的数据流事件监听器中
     * 描述：
     * 创建人：typ
     * 创建时间：2018/9/25 16:36
     * 修改人：
     * 修改描述：
     * 修改时间：
     */
    @StreamListener(Sink.INPUT)
    public void receive(String str){
        log.info("Received:" + str);
    }
}

package com.example.demo.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamSourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamSourceApplication.class, args);
	}
}

package com.example.demo.stream.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 路径：com.example.demo.stream.service
 * 类名：
 * 功能：生产者
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/25 15:57
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@EnableBinding(Source.class)
public class Sender {

    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    @InboundChannelAdapter(value = Source.OUTPUT,poller = @Poller(fixedDelay = "2000"))
    public String send(){
        String str = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        log.info("send message:"+str);
        return str;
    }
}

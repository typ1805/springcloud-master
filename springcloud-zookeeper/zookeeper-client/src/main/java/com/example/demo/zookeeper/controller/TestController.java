package com.example.demo.zookeeper.controller;

import com.example.demo.zookeeper.service.FeignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * 路径：com.example.demo.zookeeper.controller
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/25 21:43
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@RestController
public class TestController {

    private static final Logger log = LoggerFactory.getLogger(TestController.class);

    private static final UUID INSTANCE_UUID = UUID.randomUUID();

    @Autowired
    private FeignService feignService;

    @GetMapping("/test")
    public String test(){
        log.info("test :" +INSTANCE_UUID.toString());
        return feignService.test();
    }
}

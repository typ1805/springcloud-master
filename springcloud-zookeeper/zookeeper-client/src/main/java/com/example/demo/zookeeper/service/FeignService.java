package com.example.demo.zookeeper.service;

import com.example.demo.zookeeper.service.fallback.FeignFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 路径：com.example.demo.zookeeper.service
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/25 21:39
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@FeignClient(value = "zookeeper-server",fallback = FeignFallback.class)
public interface FeignService {

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public String test();
}

package com.example.demo.zookeeper.service.fallback;

import com.example.demo.zookeeper.service.FeignService;
import org.springframework.stereotype.Component;

/**
 * 路径：com.example.demo.zookeeper.service.fallback
 * 类名：
 * 功能：《用一句描述一下》
 * 备注：
 * 创建人：typ
 * 创建时间：2018/9/25 21:49
 * 修改人：
 * 修改备注：
 * 修改时间：
 */
@Component
public class FeignFallback implements FeignService {

    @Override
    public String test() {
        return "servie error!";
    }
}

package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

import java.util.Arrays;

@SpringBootApplication
@EnableAuthorizationServer
public class SecurityOauth2Application {

	/**
	 * 方法名：
	 * 功能：《用一句话描述一下》
	 * 描述： 访问路径：http://localhost:8081/oauth/authorize?client_id=client&response_type=code&redirect_uri=http://www.baidu.com
	 * 创建人：typ
	 * 创建时间：2018/9/28 17:41
	 * 修改人：
	 * 修改描述：
	 * 修改时间：
	 */
	public static void main(String[] args) {
		SpringApplication.run(SecurityOauth2Application.class, args);
	}

	@Autowired
	private AuthenticationProvider authenticationProvider;

	@Bean
	public AuthenticationManager authenticationManager(){
		return new ProviderManager(Arrays.asList(authenticationProvider));
	}
}
